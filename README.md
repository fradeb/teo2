In questa repository sono raccolte le soluzioni di diversi esercizi proposti dal prof. Vicari nel corso di Fisica Teorica 2.

* [Rinormalizzazione nello schema MS di una teoria $\phi^4$ multiparametrica](https://gitlab.com/fradeb/teo2/-/blob/main/O(N)/esercizio.pdf)
* [Correlatori e risposta lineare in sistemi many-body](https://gitlab.com/fradeb/teo2/-/blob/main/correlations/esercizio.pdf)
* [Ground-state di un gas di elettroni interagenti](https://gitlab.com/fradeb/teo2/-/blob/main/groundstate/esercizio.pdf)
* [Limite a grande N di una teoria O(N)](https://gitlab.com/fradeb/teo2/-/blob/main/largeN/esercizio.pdf)
* [Rinormalizzazione alla Wilson di una teoria O(N)](https://gitlab.com/fradeb/teo2/-/blob/main/wilson/esercizio.pdf)
